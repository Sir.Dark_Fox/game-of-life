﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Game_Of_Life
{
    public partial class MainWindow : Window
    {
        private bool _isProcessing = false;
        public ObservableCollection<ObservableCollection<Cell>> GameMatrix { get; set; }
        public int Size { get; set; } = 30;


        public MainWindow()
        {
            InitializeComponent();
            Init();
        }

        public void Init()
        {
            GameMatrix = new ObservableCollection<ObservableCollection<Cell>>();
            for (int i = 0; i < Size; i++)
            {
                GameMatrix.Add(new ObservableCollection<Cell>());
                GameField.RowDefinitions.Add(new RowDefinition());
                GameField.ColumnDefinitions.Add(new ColumnDefinition());
                for (int j = 0; j < Size; j++)
                {
                    GameMatrix[i].Add(new Cell());
                    GameMatrix[i][j].Click += cell_Click;
                    Grid.SetRow(GameMatrix[i][j], i);
                    Grid.SetColumn(GameMatrix[i][j], j);
                    GameField.Children.Add(GameMatrix[i][j]);
                }
            }
        }

        public void Clear()
        {
            _isProcessing = false;
            Generation.Content = 0;
            Warning.Visibility = Visibility.Visible;
            Start.IsEnabled = false;
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    GameMatrix[i][j].State = 0;
                    GameMatrix[i][j].NextState = 0;
                    GameMatrix[i][j].SetColor();
                }
            }
        }

        public void Rebuild()
        {
            GameMatrix = null;
            Generation.Content = 0;
            GameField.Children.Clear();
            GameField.ColumnDefinitions.Clear();
            GameField.RowDefinitions.Clear();
            Warning.Visibility = Visibility.Visible;
            Start.IsEnabled = false;
            Init();
        }


        /////////////////////
        #region Classic Game Of Life
        private void Calculate()
        {
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    int neighbors = CountNeighbors(GameMatrix[i][j], i, j);
                    if (neighbors < 2 || neighbors > 3)
                    {
                        GameMatrix[i][j].NextState = 0;
                    }
                    else if (neighbors == 3)
                    {
                        GameMatrix[i][j].NextState = 1;
                    }
                }
            }
        }
        private void Update()
        {
            int temp = 0;
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    GameMatrix[i][j].State = GameMatrix[i][j].NextState;
                    GameMatrix[i][j].SetColor();
                    temp += GameMatrix[i][j].State;
                }
            }
            Generation.Content = temp;
        }

        private int CountNeighbors(Cell cell, int x, int y)
        {
            int neighbors = 0;
            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    if (i == 0 && j == 0)
                        continue;

                    neighbors += GameMatrix[(x + i + Size) % Size][(y + j + Size) % Size].State;
                }
            }

            return neighbors;
        }
        #endregion
        /////////////////////

        private async void start_Click(object sender, RoutedEventArgs e)
        {
            _isProcessing = !_isProcessing;

            while (_isProcessing)
            {
                await Task.Run(() => Calculate());
                await Task.Delay(100);
                Update();
            }
        }
        private void clear_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }
        private void settings_Click(object sender, RoutedEventArgs e)
        {
            //Coming Soon
        }
        private void exit_Click(object sender, RoutedEventArgs e)
        {
            _isProcessing = false;
            Close();
        }
        private void cell_Click(object sender, RoutedEventArgs e)
        {
            if (!Start.IsEnabled)
            {
                Warning.Visibility = Visibility.Hidden;
                Start.IsEnabled = true;
            }
            Cell cell = sender as Cell;
            cell.State = (cell.State == 0) ? 1 : 0;
            cell.NextState = cell.State;
            Generation.Content = (cell.State == 0) ? Convert.ToInt32(Generation.Content) - 1 : Convert.ToInt32(Generation.Content) + 1;
            cell.SetColor();
        }
    }
}
