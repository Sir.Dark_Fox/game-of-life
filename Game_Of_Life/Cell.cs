﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;

namespace Game_Of_Life
{
    public class Cell : Button
    {
        private int state;
        private int nextState;

        public Cell()
        {
            state = 0;
            nextState = 0;
            SetColor();
        }

        public int State { get => state; set => state = value; }
        public int NextState { get => nextState; set => nextState = value; }

        public void SetColor()
        {
            switch (state)
            {
                case 0:
                    Background = Brushes.Black;
                    break;
                case 1:
                    Background = Brushes.White;
                    break;
            }
        }
    }
}
